module dashboard

go 1.16

require (
	github.com/gofiber/fiber/v2 v2.12.0
	github.com/robfig/cron v1.2.0
	gorm.io/driver/sqlite v1.1.4
	gorm.io/gorm v1.21.10
)
