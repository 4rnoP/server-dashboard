# Sysody Dashboard
This is my attempt to create a dashboard to access and monitor my (hopefully) running webservices.

For each service, it periodically sends a HTTP request to check if the service is running correctly.
The status of the service is shown on the dashboard, and you can force-refresh it by clicking on the status dot.

A service is described by:
- A name
- A URL
- A short description

Also, you should get for each service a small recap of the downtime duration for the day/week/month/year
## Screenshot
![](./screenshot.png)
As you can see, a lot can and has still to be done.

## How to run
```shell
git clone git@gitlab.com:4rnoP/server-dashboard.git
cd server-dashboard
go get .
# To transpile the frontend
cd vue
npm install
npm run build
cd ..
# Launch it
go run .
```
