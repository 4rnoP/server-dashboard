package main

import (
	"encoding/json"
	"fmt"
	"github.com/gofiber/fiber/v2"
	"github.com/robfig/cron"
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
	"io/ioutil"
	"log"
	"net/http"
	"strconv"
	"time"
)

type Service struct {
	ID         int
	Name       string
	Desc       string
	Link       string
	Status     bool
	StatusDesc string
	LastCheck  time.Time
}

type ServiceDowntime struct {
	gorm.Model
	Mid       int
	Today     time.Duration
	ThisWeek  time.Duration
	ThisMonth time.Duration
	ThisYear  time.Duration
}

var sites []Service

var checkInterval, _ = time.ParseDuration("5min")

var db *gorm.DB

func init() {
	content, err := ioutil.ReadFile("./services.json")
	if err != nil {
		log.Fatal("Error when opening file: ", err)
	}
	// Now let's unmarshall the data into `payload`
	err = json.Unmarshal(content, &sites)
	if err != nil {
		log.Fatal("Error during Unmarshal(): ", err)
	}
	for id, _ := range sites {
		sites[id].ID = id
		sites[id].Status = true
	}

	db, err = gorm.Open(sqlite.Open("test.db"), &gorm.Config{})
	if err != nil {
		panic("failed to connect database")
	}
	// Migrate the schema
	db.AutoMigrate(&ServiceDowntime{})
	for id, _ := range sites {
		var dt ServiceDowntime
		result := db.Where("mid = ?", id).First(&dt)
		log.Println(id)
		if result.Error != nil {
			db.Create(&ServiceDowntime{
				Mid:       id,
				Today:     0,
				ThisWeek:  0,
				ThisMonth: 0,
				ThisYear:  0,
			})
		}
	}

	fmt.Println("Initializing cron task...")
	c := cron.New()
	c.AddFunc("*/5 * * * * *", func() {
		checkMultipleServices(sites)
	})
	c.Start()
}

func main() {
	app := fiber.New()
	app.Static("/", "./vue/dist")

	api := app.Group("/api")
	api.Get("/getStatus", getStatus)
	api.Get("/getDowntime", getDowntime)

	app.Static("/", "./files")

	log.Print(app.Listen(":3000"))
}

func getStatus(c *fiber.Ctx) error {
	if c.Query("server") == "all" {
		res, _ := json.Marshal(sites)
		return c.Send(res)
	}
	id, err := strconv.Atoi(c.Query("server"))
	if err != nil {
		return c.SendStatus(400)
	}
	if id >= len(sites) {
		return c.SendStatus(404)
	}
	checkService(&sites[id], true)
	res, _ := json.Marshal(sites[id])
	return c.Send(res)
}

func getDowntime(c *fiber.Ctx) error {
	id, err := strconv.Atoi(c.Query("server"))
	if err != nil {
		return c.SendStatus(400)
	}
	if id >= len(sites) {
		return c.SendStatus(404)
	}
	var dt ServiceDowntime
	db.Where("mid = ?", id).Find(&dt)
	res, _ := json.Marshal(dt)
	return c.Send(res)
}

func checkMultipleServices(services []Service) {
	for id, _ := range services {
		checkService(&services[id], false)
	}
}

func checkService(service *Service, now bool) {
	if time.Since(service.LastCheck) > checkInterval || now {
		log.Printf("Checking %s\n", service.Name)
		if time.Since(service.LastCheck) > checkInterval && !service.Status {
			log.Printf("Service %s down !\n", service.Name)
			var dt ServiceDowntime
			db.Where("Mid = ?", service.ID).First(&dt)
			db.Model(&dt).Updates(ServiceDowntime{
				Today:     checkInterval + dt.Today,
				ThisWeek:  checkInterval + dt.ThisWeek,
				ThisMonth: checkInterval + dt.ThisMonth,
				ThisYear:  checkInterval + dt.ThisYear,
			})
		}
		resp, err := http.Get(service.Link)
		if err != nil {
			service.StatusDesc = "Offline"
			service.Status = false
		} else {
			service.StatusDesc = "Code " + string(fmt.Sprint(resp.StatusCode))
			if resp.StatusCode < 300 || resp.StatusCode == 401 {
				service.Status = true
			} else {
				service.Status = false
			}
		}
		service.LastCheck = time.Now()
	}
}
